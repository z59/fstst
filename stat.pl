#!/usr/bin/perl

my %stat, $total = 0, $data, $key;

while (read (STDIN, $data, 8) == 8) {
	$key = unpack('Q<', $data);
	if ($key > 0) {
		$stat{ $key }++;
		$total += $key
	}
}
for $key (sort { $b <=> $a } keys %stat) {
	print $key, ' writes: ', $stat{$key}, " sectors<br>";
}
print 'Total: ', $total, ' writes';
