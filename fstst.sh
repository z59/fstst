#!/bin/sh
set -o errexit    # exit on error
set -o nounset    # fail if var undefined
set -o noclobber  # don't overwrite existing files via >


FULL_TEST=0
SIZE=$(( 2 * 1024 * 1024 * 1024 ))  # 2 GiB
FSTST_DIR=/tmp/fstst


create() {
	truncate -s "$SIZE" "$FSTST_DIR/img"
	truncate -s "$(( $SIZE / 512 * 8 ))" "$FSTST_DIR/img-stat"
	./fuseloop -o allow_other "$FSTST_DIR/img" "$FSTST_DIR/fuse"
	head -c 1 "$FSTST_DIR/fuse" >/dev/null  # init fuseloop before setting up loopdev
	LOOP=$(losetup -f --show "$FSTST_DIR/fuse")
}
remove() {
	losetup -d "$LOOP"
	umount "$FSTST_DIR/fuse"
	rm -f "$FSTST_DIR/img-stat" "$FSTST_DIR/img"
}
stats_reset() {
	dd if=/dev/zero "of=$FSTST_DIR/img-stat" "bs=$(( $SIZE / 512 ))" count=8 conv=notrunc status=none
}
stats() {
	sync
	sleep 1
	./stat.pl <"$FSTST_DIR/img-stat"
	printf '|'
	stats_reset
}
test_fs() {
	printf %s "$FSTYPE<br>$MKFS_OPT<br>$MNT_OPT|"
	create
	(
		if [ "$FSTYPE" = vfat ]; then
			"mkfs.$FSTYPE" $MKFS_OPT "$LOOP" >/dev/null
		else
			"mkfs.$FSTYPE" $MKFS_OPT "$LOOP"
		fi
		stats
		mount -t "$FSTYPE" $MNT_OPT "$LOOP" "$FSTST_DIR/mnt"
		umount "$FSTST_DIR/mnt"
		stats
		mount -t "$FSTYPE" $MNT_OPT "$LOOP" "$FSTST_DIR/mnt"
		umount "$FSTST_DIR/mnt"
		stats
		if [ -n "${FULL_TEST-}" ]; then
			mount -t "$FSTYPE" $MNT_OPT "$LOOP" "$FSTST_DIR/mnt"
			umount "$FSTST_DIR/mnt"
			stats
		fi
		mount -t "$FSTYPE" $MNT_OPT "$LOOP" "$FSTST_DIR/mnt"
		stats_reset
		i=0; while [ "$i" -lt 1024 ];  do
			dd if=/dev/zero "of=$FSTST_DIR/mnt/test.img" bs=1M count=1 conv=notrunc status=none
			sync
			i=$((i+1))
		done
		stats
		umount "$FSTST_DIR/mnt"
		stats_reset
		i=0; while [ "$i" -lt 1024 ]; do
			mount -t "$FSTYPE" $MNT_OPT "$LOOP" "$FSTST_DIR/mnt"
			umount "$FSTST_DIR/mnt"
			i=$((i+1))
		done
		stats
	) || :
	echo
	remove
}


[ "${FULL_TEST-}" = 1 ] || FULL_TEST=
mkdir -p "$FSTST_DIR/mnt"
[ -e "$FSTST_DIR/fuse" ] || >"$FSTST_DIR/fuse"
cat <<EOD
# $(uname -rs)

fs|mkfs|mount-umount1|mount-umount${FULL_TEST:+2|mount-umount3}|1024 rewrites|1024 remounts
-|-|-|-${FULL_TEST:+|-}|-|-
EOD

FSTYPE=nilfs2 MKFS_OPT=-q MNT_OPT='-o noatime' test_fs
FSTYPE=f2fs MKFS_OPT=-q MNT_OPT='-o noatime,mode=lfs' test_fs
FSTYPE=btrfs MKFS_OPT=-q MNT_OPT='-o noatime' test_fs
FSTYPE=ext4 MKFS_OPT=-q MNT_OPT='-o noatime' test_fs
FSTYPE=vfat MKFS_OPT= MNT_OPT='-o noatime' test_fs
# FSTYPE=xfs MKFS_OPT=-q MNT_OPT='-o noatime' test_fs

rm -rf "$FSTST_DIR"
